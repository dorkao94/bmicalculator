import * as React from 'react';
import './App.css';
import BMI from './components/BMI';



export interface Props {
  weight?: number;
  height?: number;
}

export interface State {
  weight?: number;
  height?: number;
}

class App extends React.Component<Props,State>{
  constructor(props:Props) {
    super(props);
    this.state = {
        weight: this.props.weight,
        height: this.props.height
    };
  }
  public handleOnChange(e: any) : void {
    this.setState({[e.target.name]: e.target.value});
  }

  public render() {
    return (
      <div className="App">
        <form name="bmiForm"  >
          Your Weight(kg): <input type="text" className="input" name="weight" onChange={ e => this.handleOnChange(e)}/><br />
          Your Height(cm): <input type="text" className="input" name="height" onChange={ e => this.handleOnChange(e)} /><br />
          <BMI weight={this.state.weight? this.state.weight: 10} height={this.state.height?this.state.height: 10} />
        </form>
      </div>
    );
  }
}

export default App;
