import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
// import { createStore } from 'redux';
// import { calculate } from './reducers/index';
// import { StoreState } from './types/index';
// import { Provider } from 'react-redux';

// const store = createStore<StoreState>(calculate, {
//   weight: 10,
//   height: 10,
//   bmi: 0
// });

// ReactDOM.render(
//   <Provider store={store}>
//   <App />
//   </Provider>,
//   document.getElementById('root') as HTMLElement
// );
ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
