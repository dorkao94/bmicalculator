export interface StoreState {
    weight?: number;
    height?: number;
    bmi?: number;
}