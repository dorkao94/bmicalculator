import { BMIAction } from '../actions';
import { StoreState } from '../types/index';
import { BMI_CALCULATOR } from '../constants/index';

export function calculate(state: StoreState, action: BMIAction): StoreState {
  switch (action.type) {
    case BMI_CALCULATOR:
    //   return { ...state, bmi: state.weight/(state.height/100*state.height/100) };
    return { ...state, bmi: state.bmi };
  }
  return state;
}