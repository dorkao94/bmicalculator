import * as React from 'react';

export interface Props {
  weight: number;
  height: number;
}

const THIN = 18.5;
const OVERWEIGHT = 25;
const THIN_MSG = "that you are too thin!";
const HEALTHY_MSG = "that you are healthy!";
const OVERWEIGHT_MSG = "that you are overweight!";
const ERROR_MSG = "I dont think your weight and height are equal to 0 :D";

class BMI extends React.Component<Props, object> {
    private bmiCalculate(weight:number, height:number){
      return  weight/(height/100*height/100);
    }

    render() {
      const { weight, height } = this.props;
      let finalBmi = this.bmiCalculate(weight, height);
      

      if (weight <= 0 && height <= 0) {
        throw new Error(ERROR_MSG);
      }
      let message = "";
      if(finalBmi < THIN){
        message = THIN_MSG;
      }

      if(finalBmi > THIN && finalBmi < OVERWEIGHT){
          message = HEALTHY_MSG;
      }

      if(finalBmi > OVERWEIGHT){
        message = OVERWEIGHT_MSG;
      }

      return (
        <div className="hello">
          <div className="bmi">
            Your BMI: {finalBmi}
            <br/>
            This means: {message}
          </div>
        </div>
      );
    }
}

export default BMI;

