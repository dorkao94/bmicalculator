import * as constants from '../constants';

export interface BMICalculator {
    type: constants.BMI_CALCULATOR;
}

export interface ChangeHeight {
    type: constants.CHANGE_HEIGHT;
}

export type BMIAction = BMICalculator;

export function bmiCalculator(): BMICalculator {
    return {
        type: constants.BMI_CALCULATOR
    }
}

export function changeHeight(): ChangeHeight {
    return {
        type: constants.CHANGE_HEIGHT
    }
}